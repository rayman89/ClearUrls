Welcome to the **ClearURLs** wiki!

Here we are trying to explain the operation and functionality of **ClearURLs**. If you cannot find an answer to a question about operation or functionality, please write us a note in the issues.