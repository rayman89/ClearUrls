# ClearURLs [![Mozilla Add-on](https://img.shields.io/amo/v/clearurls.svg)](https://addons.mozilla.org/en-US/firefox/addon/clearurls/versions/)
[![Donate](https://img.shields.io/badge/donate-PayPal-blue.svg)](https://www.paypal.me/KevinRoebert)

[![Mozilla Add-on](https://img.shields.io/amo/stars/clearurls.svg)](https://addons.mozilla.org/en-US/firefox/addon/clearurls/reviews/)

[![Mozilla Add-on](https://img.shields.io/amo/users/clearurls.svg)](https://addons.mozilla.org/en-US/firefox/addon/clearurls/statistics/?last=30)

**ClearURLs** is an add-on based on the new WebExtensions technology and is optimized for *Firefox*.

This add-on will remove the tracking fields from all URLs which are visited by the browser and use a rule file, namely `data.json`.

This add-on protects your privacy and block the request from advertising services like *doubleclick.net*.

## Application
Large (and small) webpages use elements in the URL, e.g.: https://example.com?source=thisIstheSiteIvisitedBefore to track your online activities. In this example, the source field tells the provider which page you visited before. The add-on will remove these tracking fields from the URL.

## Installation
[![Mozilla Add-on](https://img.shields.io/amo/d/clearurls.svg)](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)

## Contribute
If you have any suggestions or complaints, please [create an issue.](https://gitlab.com/KevinRoebert/ClearUrls/issues/new)

## Screenshot
![Interface (version 1.2.2.x)](https://gitlab.com/KevinRoebert/ClearUrls/raw/48b8b9e994eb2535b11106dcd766097d55b493dd/promotion/screens/Popup_v_1.2.2.8.png)

## Copyright
We use some third-party scripts in our add-on. The authors and licenses are listed below.
-   [Bootstrap v3.3.7 ](http://getbootstrap.com) |
    Copyright 2011-2016 Twitter, Inc. |
    [MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE)
-   [jQuery v3.2.1](https://jquery.com/) |
    Copyright 2017 The jQuery Foundation |
    [MIT](https://jquery.org/license/)
-   [sha256.jquery.plugin](https://github.com/orsozed/sha256.jquery.plugin) |
    Copyright 2003, Christoph Bichlmeier |
    [MIT](https://raw.github.com/orsozed/JQuery-Plugins/master/license/MIT-LICENSE.txt) |
    [GPLv2](https://raw.github.com/orsozed/JQuery-Plugins/master/license/GPL-LICENSE.txt)
-   [DataTables](https://datatables.net/) |  Copyright 2011-2015 SpryMedia Ltd | [MIT](https://datatables.net/license/)
-   [Pick-a-Color v1.2.3](https://github.com/lauren/pick-a-color) | Copyright (c) 2013 Lauren Sperber and Broadstreet Ads |
    [MIT](https://github.com/lauren/pick-a-color/blob/master/LICENSE)

## Licence
[![License: QaPL v0.2](https://img.shields.io/badge/License-QaPL%20v0.2-brightgreen.svg)](https://gitlab.com/KevinRoebert/ClearUrls/blob/master/LICENSE.md)
